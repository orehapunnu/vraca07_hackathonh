﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallRaycast : MonoBehaviour
{
    public float minControllerAngle = 60f;
    public float maxControllerAngle = 120f;
    public float maxDistance = 20f;

    [SerializeField] Transform trackingSpace;
    private OVRInput.Controller Controller;     // コントローラ_右手

    [SerializeField] MeshRenderer cubeMaterial;

    public Vector3 ControllerForward
    {
        get
        {
            Quaternion orientation = OVRInput.GetLocalControllerRotation(Controller);
            Vector3 worldForward = trackingSpace.localToWorldMatrix.MultiplyVector(orientation * Vector3.forward);

            return worldForward.normalized;
        }
    }

    public Vector3 Up { get { return trackingSpace.up; } }

    Ray HorizontalRay
    {
        get
        {
            Vector3 horizontalDirection = Vector3.ProjectOnPlane(ControllerForward, Up);
            Vector3 playerPosition = trackingSpace.position;
            return new Ray(playerPosition, horizontalDirection);
        }
    }

    public float HorizontalDistance
    {
        get
        {
            float controllerAngle = Vector3.Angle(Up * -1.0f, ControllerForward);
            float pitch = Mathf.Clamp(controllerAngle, minControllerAngle, maxControllerAngle);
            float pitchRange = maxControllerAngle - minControllerAngle;
            float t = (pitch - minControllerAngle) / pitchRange; // Normalized pitch within range
            return maxDistance * t;
        }
    }
    Vector3 PointAlongHorizontalRay
    {
        get
        {
            return HorizontalRay.origin + HorizontalRay.direction * HorizontalDistance;
        }
    }









    void Start()
    {
        // コントローラ_右手の取得
        Controller = OVRInput.Controller.RTouch;
    }

    void Update()
    {
        // Rayを飛ばす
        Ray ray = HorizontalRay;
        // outパラメータ用に、Rayのヒット情報を取得するための変数を用意
        RaycastHit hit;

        // シーンビューにRayを可視化するデバッグ（必要がなければ消してOK）
        Debug.DrawRay(ray.origin, ray.direction * 30.0f, Color.red, 0.0f);

        // Rayのhit情報を取得する
        if (Physics.Raycast(ray, out hit, 30.0f))
        {

            // Rayがhitしたオブジェクトのタグ名を取得
            string hitTag = hit.collider.tag;

            // タグの名前がEnemyだったら、照準の色が変わる
            if ((hitTag.Equals("Cube")))
            {
                //cubeを赤に変える
                cubeMaterial.material.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
            }
            else
            {
                // Cube以外では水色に
                cubeMaterial.material.color = new Color(0.0f, 1.0f, 1.0f, 1.0f);
            }

        }
        else
        {
            // Rayがヒットしていない場合は水色に
            cubeMaterial.material.color = new Color(0.0f, 1.0f, 1.0f, 1.0f);
        }

    }
}
